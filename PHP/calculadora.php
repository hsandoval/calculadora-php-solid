<?php
	require_once "ioperacion.php";
	require_once "suma.php";
	require_once "resta.php";
	require_once "multiplicacion.php";
	require_once "division.php";
	
	class Calculadora
	{
		private $OperandoUno;
		private $OperandoDos;
		private $Resultado;
		private $OperacionMatematica;

		function __construct($operacion, $operandoUno, $operandoDos){
			$this->OperacionMatematica = $operacion;
			$this->OperandoUno = $operandoUno;
			$this->OperandoDos = $operandoDos;
			$this->DeterminarOperacion();
		}
		
		function ObtenerResultado(){
			$this->Calcular();
			return $this->Resultado;
		}

		private function DeterminarOperacion() {
			switch ($this->OperacionMatematica) {
				case 'sumar':
					$this->Operacion = new Suma($this->OperandoUno, $this->OperandoDos);
					break;
				case 'restar':
					$this->Operacion = new Resta($this->OperandoUno, $this->OperandoDos);
					break;
				case 'multiplicar':
					$this->Operacion = new Multiplicacion($this->OperandoUno, $this->OperandoDos);
					break;											
				case 'dividir':
					$this->Operacion = new Division($this->OperandoUno, $this->OperandoDos);
					break;
			}
		}

		private function Calcular(){
			$this->Resultado = $this->Operacion->Calcular();
		}
	}
?>