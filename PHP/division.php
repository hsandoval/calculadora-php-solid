<?php
	include_once "operacion.php";
	include_once "ioperacion.php";

	class Division extends Operacion implements IOperacion
	{
		function Calcular(){
			if ($this->OperandoDos == 0) {
				throw new Exception('División por cero.');
			}
			return $this->OperandoUno / $this->OperandoDos;
		}
	}	
?>