<?php
	include_once "operacion.php";
	include_once "ioperacion.php";

	class Resta extends Operacion implements IOperacion
	{
		function Calcular(){
			return $this->OperandoUno - $this->OperandoDos;
		}	
	}	
?>