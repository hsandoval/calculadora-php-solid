const calcular = async (event) => {
	event.preventDefault();
	const form = document.getElementById('frmOperacion');
	let jsonData = {};

	for (const pair of new FormData(form)) {
		jsonData[pair[0]] = pair[1];
	}

	const respuesta = await enviarPeticion('controller.php', jsonData);
	document.getElementById('lblResultado').value = respuesta.body.resultado;

}

const enviarPeticion = (action, jsonBody) => {
	const URL = `${document.location}PHP/${action}`;
	return new Promise(async (resolve, reject) => {
		const rawResponse = await fetch(URL, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(jsonBody)
		})
			.then(response => response.text().then(data => {
				return ({ status: response.status, body: JSON.parse(data) })
			}
			))
			.then((data) => resolve(data))
			.catch(err => reject(Error(err.message)));
	});
};