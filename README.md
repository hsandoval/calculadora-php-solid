# Calculadora-PHP-SOLID ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

Aplicación básica de PHP con aplicación de principales principios de la programación orientada a objetos y principios de desarrollo SOLID.
Diseñada mediante PHP Puro para funcionar como una API con retorno de resultados

## Installation

Es necesario llevar el proyecto a la carpeta requerida por el server PHP y realizar las pruebas desde la plantilla HTML proporcionada.

Tambien es posible realizar pruebas con el REST API Testing de preferencia.

## Requirements

* HTML 5
* Vanilla Javascript 
* PHP 7.2

## License
[MIT](https://choosealicense.com/licenses/mit/)